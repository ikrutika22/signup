const express = require('express')
const app = express()
const cors = require('cors')
app.use(cors())
var port = process.env.PORT || 8080;

app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))

const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-varmak2:Krutika22@cca-varmak2.ju44h.mongodb.net/assignment?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser:true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});

app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port:${port}`))

app.get('/', (req, res) => {
    res.send("SignUp by varmak2 CCA.") });

app.get('/signuptest', (req, res) => { 
        res.sendFile(__dirname + '/sign.html')
    });

app.post('/signup', (req, res) => {
    var fname = req.body['fname']
    var lname = req.body['lname']
    var uname = req.body['uname']
    var email = req.body['email']
    var pwd = req.body['pwd']

    console.log("In Signup microservice: ",fname, lname, uname, email, pwd)

    const db = dbClient.db("assignment");

    db.collection("users").findOne({email:email}, (err, result) =>
    {
        if(result)
        {
            console.log("Microservice-Email already exists.");
            res.send({"Status":false, "message":"Email already exists."});
            return;
        }     
        else
        {
            db.collection("users").findOne({uname:uname}, (err, result) =>
            {
                if(result)
                {
                    console.log("Microservice-Username already exists.");
                    res.send({"Status": false, "message": "Username already exists."});
                    return;
                }
                else{
                    let newUser = {fname: fname , lname: lname, uname: uname, email: email, pwd: pwd}
                    
                    db.collection("users").insertOne( newUser, (err, result) =>
                    {
                        if(err)
                        {
                            console.log("Microservice-Error");                            
                            res.send({"Status": false, "message": err});
                            return;
                        }
                        else
                        {
                            console.log("SignUp Successful!");
                            res.send({"Status": true, "message": "Signup Successful."});
                            return;
                        }
                    })                    
                }
            })
        }   
    })
});